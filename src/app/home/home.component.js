/******************************************************************
Home  component 
Author : Aashir Javed
controller as vm
******************************************************************/


;
(function() {

    'use strict';

    angular
        .module('aashirMarkinsonAssesment')
        .component('home', {
            templateUrl: 'app/home/home.html',
            controllerAs: 'vm',
            controller: HomeCtrl
        });


    HomeCtrl.$inject = ['$scope', 'QueryService'];
    //for finding the most common words
    //How to use it: nthMostCommon('Hello find the words inside me Hello find Hello',3)
    //will return the array containing words with occurrances
    function nthMostCommon(string, ammount) {
        var wordsArray = string.split(/\s/);
        var wordOccurrences = {}
        for (var i = 0; i < wordsArray.length; i++) {
            wordOccurrences['_' + wordsArray[i]] = (wordOccurrences['_' + wordsArray[i]] || 0) + 1;
        }
        var result = Object.keys(wordOccurrences).reduce(function(acc, currentKey) {
            /* you may want to include a binary search here but linear is just fine */
            for (var i = 0; i < ammount; i++) {
                if (!acc[i]) {
                    acc[i] = { word: currentKey.slice(1, currentKey.length), occurences: wordOccurrences[currentKey] };
                    break;
                } else if (acc[i].occurences < wordOccurrences[currentKey]) {
                    acc.splice(i, 0, { word: currentKey.slice(1, currentKey.length), occurences: wordOccurrences[currentKey] });
                    if (acc.length > ammount)
                        acc.pop();
                    break;
                }
            }
            return acc;
        }, []);
        return result;
    }

    function HomeCtrl($scope, QueryService) {
        $scope.CompanyString = "";
        $scope.uniqueAllComapnyNames = [];
        QueryService.query('GET', 'Customer').
        then((response) => {
            response.data.forEach(item => {
                $scope.CompanyString = $scope.CompanyString + " " + item.companyName
                if ($scope.uniqueAllComapnyNames.indexOf(item.companyName) > -1) {
                    //do nothing
                } else {
                    $scope.uniqueAllComapnyNames.push(item.companyName)
                }
            })
            $scope.words = nthMostCommon($scope.CompanyString, 5);


        }, (err) => {
            alert(err)
            console.log(err)
        })



    }

})();