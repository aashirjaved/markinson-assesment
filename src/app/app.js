;
(function() {

    /**
     * Definition of the main app module and its dependencies
     */
    angular
        .module('aashirMarkinsonAssesment', [
            'ui.router',
            'ngDialog'
        ])
        .config(config);

    config.$inject = ['$locationProvider', '$httpProvider', '$logProvider'];

    /**
     * App config
     */
    function config($locationProvider, $httpProvider, $logProvider) {


        $logProvider.debugEnabled(true);

    }

    /**
     * Run block
     */
    angular
        .module('aashirMarkinsonAssesment')
        .run(run);

    run.$inject = ['$transitions', '$location', '$state', '$rootScope'];

    function run($transitions, $location, $state, $rootScope, localStorage) {
        // this runs on every route change
        $transitions.onStart({}, function(trans) {
            trans.promise.then(function(state) {

            });
        });



    }

})();